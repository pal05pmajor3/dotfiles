#My fish config.

###EXPORT###
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths
set fish_greeting		#Suppresses intro message
set TERM "xterm-256color"	#Terminal type
set EDITOR "vim"		#$EDITOR to vim in terminal
set VISUAL "notepadqq"		#$VISUAL to notepadqq in GUI

### SET MANPAGER

###"bat" as manpager
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

###FUNCTIONS###

#Functions for !! and !$
function __history_previous_command 
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$' 
  end
end
#Bindings for !! and !$
if [ $fish_key_bindings = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

### END OF FUNCTIONS###

### ALIAS LIST ###


##Package manaement
alias pacup='sudo pacman -Syu'
alias pacin='sudo pacman -S'
alias pacrm='sudo pacman -Rs'
alias parup='paru -Sua'

#interactivety when using cp, mv or rm
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'
#Lsd commands
alias ls='lsd -lah'
alias la='lsd -a'
#alias to change brightness
alias br='xrandr --output HDMI-A-0 --brightness'
#Cat alias
alias cat='bat'
#Terminal programs alias
alias mail='neomutt'
alias rss='newsboat'
alias web='lynx'
alias music='mocp'

#Git bare repository alias
alias config='/usr/bin/git --git-dir=/home/paul/dotfiles --work-tree=/home/paul'

#HDMI aliases
alias hdmion='xrandr --output HDMI1 --auto --right-of LVDS1'
alias hdmioff='xrandr --output HDMI1 --off'

## Startx aliases

alias starto='startx /usr/bin/openbox-session'

### END OF ALIASES ###

###RANDOM COLOR SCRIPT###
colorscript random

### STARSHIP PROMPT ###
starship init fish | source
