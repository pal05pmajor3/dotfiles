#Imports
import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile import hook
from libqtile import qtile
from typing import List  # noqa: F401
from libqtile.backend.wayland import InputConfig

#Variables
mod = "mod4"
myTerm = "alacritty"
myWeb = "brave"
myWeb2 = "qutebrowser"
myFile= "pcmanfm"
myMail= "thunderbird"

#Key Bindings
keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "space", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    ###Run Applications
    #Open a terminal
    Key([mod], "Return", lazy.spawn(myTerm+" -e zsh"), desc="Launch terminal"),
    
    #Open a web browser
    Key([mod], "w", lazy.spawn(myWeb), desc="Launch Web Browser"),
    Key([mod, "shift"], "w", lazy.spawn(myWeb2), desc="Launch Web Browser 2"),

    #Open emacs
    Key([mod], "e", lazy.spawn("emacsclient -c -a 'emacs'"), desc="Launch Emacs Client"),

    #Open a graphical email client
    Key([mod, "Shift"], "e", lazy.spawn("thunderbird"), desc="Launch Email Client"),

    #Run Rofi drun
    Key ([mod, "shift"], "Return", lazy.spawn("wofi"), desc="Launch rofi drun"),

    #Run Rofi Window
    #Run File Manager
    Key ([mod], "f", lazy.spawn(myFile), desc="Launch File Manager"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle Floating"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

group_names = [("1.Main", {'layout': 'monadtall'}),
               ("2.Web", {'layout': 'monadtall'}),
               ("3.VM", {'layout': 'monadtall'}),
               ("4.Mus", {'layout': 'monadtall'}),
               ("5.Vid", {'layout': 'monadtall'}),
               ("6.Docs", {'layout': 'monadtall'}),
               ("7.Sys", {'layout': 'monadtall'}),
               ("8.Soc", {'layout': 'monadtall'}),
               ("9.GFX", {'layout': 'floating'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layouts = [
    layout.MonadTall(),
    layout.Columns(border_focus_stack='#d75f5f'),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

colors = [["#282c34", "#282c34"], # panel background
          ["#3d3f4b", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for 'other tabs' and color for 'odd widgets'
          ["#4f76c7", "#4f76c7"], # color for the 'even widgets'
          ["#e1acff", "#e1acff"]] # window name



screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                       foreground = '#179AE7',
		       scale = 0.7,
),
                widget.GroupBox(
		       hide_unused = False,
                       active = '#EEB71E',
 		       inactive = '#823481',
		       font = "Ubuntu",
),
                widget.Prompt(),
                widget.WindowName(
		       foreground = '#F61E1E',
		       font = "Ubuntu",
),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
		       font = "Ubuntu",
                ),
#                widget.Sep(
#                       linewidth = 0,
#                       padding = 4,
#                       foreground = colors[2],
#                       background = colors[0]
#                       ),
# 	        widget.Battery(
#                       format = 'Bat {char} {percent:2.0%} {hour:d}:{min:02d} {watt:.2f} W',
#                       charge_char = '↑',
#                       discharge_char = '↓',
#                       foreground = '#0DC721',
#		       font = "Ubuntu",
#		       update_interval = 15,
#),
                    
                widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.Memory(
                       format = 'Mem {MemUsed: .0f}M',
                       foreground = '#FF3FC7',
		       font = "Ubuntu",
),
                widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.CPU(
                       format = 'CPU {load_percent}%',
                       foreground = '#0DC721',
		       font = "Ubuntu",
),
                widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.ThermalSensor(
                       fmt = 'Temp {}',
                       foreground = '#FF3FC7',
		       font = "Ubuntu",
),
                widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0]
                       ),
                widget.Clock(format='%A, %d %B - %H:%M',
                       foreground = '#0DC721',
		       font = "Ubuntu",
),

                widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0],
                       ),
#                widget.Systray(),
#                 widget.StatusNotifier(),
            ],
            24,
        ),
    ),
]


# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

#Autostart 
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
